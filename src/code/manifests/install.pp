class cernbox::install {

  if ($::operatingsystemmajrelease == '7') {
    yumrepo{'cernbox':
      descr         => 'CERNBOX2.0 client repository (EOS backend)',
      baseurl       => "http://cernbox.cern.ch/cernbox/doc/Linux/repo/CentOS_${::operatingsystemmajrelease}",
      gpgcheck      => 1,
      gpgkey        => "http://cernbox.cern.ch/cernbox/doc/Linux/repo/CentOS_${::operatingsystemmajrelease}/repodata/repomd.xml.key",
      repo_gpgcheck => 0,
      enabled       => 1,
      before        => Package['cernbox-client'],
    }
  } else {
    osrepos::kojitag { 'cernbox':
      available_major_versions => [8, 9],
      description              => 'CERNBox Client repo',
      before                   => Package['cernbox-client'],
    }
  }
  package { 'cernbox-client':
    ensure => present,
  }
}
